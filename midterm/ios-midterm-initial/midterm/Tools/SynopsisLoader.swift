//
//  SynopsisLoader.swift
//  midterm
//
//  Created by Casey Goldstein on 3/2/24.
//

import Foundation

@Observable
class SynopsisLoader {
    var synopsis: String = ""
    let apiClient: BookAPIClient
    private(set) var state: LoadingState = .idle
    
    enum LoadingState{
        case idle
        case loading
        case failed (error:Error)
        case success (data: String)
    }
    
    init(apiClient: BookAPIClient) {

        self.apiClient = apiClient
    }
    
    @MainActor
    func loadSynopsis(id:String) async {
        self.state = .loading
        do{
            synopsis = try await apiClient.fetchSynopsis(id:id)
            self.state = .success(data:synopsis)
        } catch {
            self.state = .failed(error:error)
        }
    }
}
