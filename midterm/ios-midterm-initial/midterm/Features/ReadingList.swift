import SwiftUI
import SwiftData

struct ReadingList: View {
    @Environment(\.modelContext) private var modelContext
    @Query(filter:#Predicate<Book> { book in
        return book.readingList == true
    }, sort: \Book.title) private var books: [Book]
    
    
  var body: some View {
      List(books){ book in
          if (book.readingList){
              HStack{
                  AsyncImage(url:book.coverUrl){
                      $0
                          .resizable()
                          .aspectRatio(contentMode: .fit)
                          .frame(width:50,height:50)
                  } placeholder:{
                      if book.coverUrl != nil{
                          ProgressView()
                      } else {
                          Image(systemName: "book")
                              .resizable()
                              .aspectRatio(contentMode: .fit)
                              .frame(width:50,height:20)
                      }
                  }
                  VStack (alignment:.leading){
                      Text(book.title)
                          .font(.headline)
                      Text("\(book.author)")
                          .font(.subheadline)
                  }
              }
              .swipeActions(edge:.trailing){
                  Button {book.readingList.toggle() } label: {
                      Label("Delete",systemImage: "trash")
                  }
                  .tint(.red)
          }
                
                
                
      }
      
      

    }
  }
}

#Preview {
    
  let preview = PreviewContainer([Book.self])
  preview.add(items: Book.previewData)
    
  return
    ReadingList()
      .modelContainer (preview.container)
}
