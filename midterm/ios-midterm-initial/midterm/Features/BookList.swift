import SwiftUI
import SwiftData

struct BookList: View {
    @Environment(\.modelContext) private var modelContext
    @Query(sort: \Book.title) private var books: [Book]

    @State private var isCreateFormPresented: Bool = false
    @State private var newBookForm = Book.FormData()

    var body: some View {
        List(books) { book in
            NavigationLink(destination: BookDetail(book: book, synopsisLoader: SynopsisLoader(apiClient: BookAPIClient()))) {
                HStack {
                    AsyncImage(url: book.coverUrl) {
                        $0
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 80, height: 120)
                            .cornerRadius(8)
                            .overlay(
                                RoundedRectangle(cornerRadius: 8)
                                    .stroke(Color.gray, lineWidth: 1)
                            )
                    } placeholder: {
                        if book.coverUrl != nil {
                            ProgressView()
                                .scaleEffect(1.5)
                        } else {
                            Image(systemName: "book.fill")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 80, height: 120)
                                .foregroundColor(.gray)
                        }
                    }

                    VStack(alignment: .leading) {
                        Text(book.title)
                            .font(.title)
                            .foregroundColor(.black)
                            .multilineTextAlignment(.leading)
                            .lineLimit(2)
                            .padding(.bottom, 4)

                        Text("by \(book.author)")
                            .font(.subheadline)
                            .foregroundColor(.black)
                            .italic()
                    }
                    .padding(.leading, 8)
                }
                .padding(8)
            }
        }
        .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
                Button("Create") {
                    isCreateFormPresented.toggle()
                }
                .font(.headline)
                .foregroundColor(.blue)
            }
        }
        .sheet(isPresented: $isCreateFormPresented) {
            NavigationStack {
                BookForm(data: $newBookForm)
                    .toolbar {
                        ToolbarItem(placement: .navigationBarLeading) {
                            Button("Cancel") {
                                isCreateFormPresented.toggle()
                                newBookForm = Book.FormData()
                            }
                            .font(.subheadline)
                            .foregroundColor(.red)
                        }
                        ToolbarItem(placement: .navigationBarTrailing) {
                            Button("Save") {
                                Book.create(from: newBookForm, context: modelContext)
                                newBookForm = Book.FormData()
                                isCreateFormPresented.toggle()
                            }
                            .font(.subheadline)
                            .foregroundColor(.green)
                        }
                    }
            }
        }
        .onAppear {
            if books.isEmpty {
                for book in Book.previewData {
                    modelContext.insert(book)
                }
            }
        }
    }
}

#Preview {
    let preview = PreviewContainer([Book.self])
    preview.add(items: Book.previewData)
    return
        BookList()
            .modelContainer(preview.container)
}
