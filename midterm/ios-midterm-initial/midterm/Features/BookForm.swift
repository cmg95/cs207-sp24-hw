//
//  BookForm.swift
//  midterm
//
//  Created by Casey Goldstein on 3/4/24.
//

import Foundation
import SwiftUI

struct BookForm: View {
    @Binding var data: Book.FormData
    
    var body: some View {
        Form {
            TextFieldWithLabel(label:"Book ID",text:$data.id, prompt:"id")
                .padding(.horizontal, 10)
            TextFieldWithLabel(label:"Book Title",text:$data.title, prompt:"Title")
                .padding(.horizontal, 10)
            TextFieldWithLabel(label:"Book Author",text:$data.author,prompt:"Author")
                .padding(.horizontal, 10)
            
        }
    }
}

#Preview {
    let preview = PreviewContainer([Book.self])
    let data = Binding.constant(Book.previewData[0].dataForForm)
    
    return BookForm(data:data)
        .modelContainer(preview.container)
}
