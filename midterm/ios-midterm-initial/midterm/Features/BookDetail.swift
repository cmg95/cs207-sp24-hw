//
//  BookDetail.swift
//  midterm
//
//  Created by Casey Goldstein on 3/4/24.
//

import Foundation
import SwiftUI

struct BookDetail: View {
    
    @State var book: Book
    @State var synopsisLoader: SynopsisLoader
    
    var body: some View{
        
        ScrollView{
            AsyncImage(url: book.coverUrl){
                $0
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 150, height:250)
            } placeholder: {
                if book.coverUrl != nil{
                    ProgressView()
                } else {
                    Image(systemName:"book")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width:150,height:250)
                }
            }
            Text(book.title)
                .font(.system(size: 20, weight: .bold, design: .default))
                .background(Color.yellow)
            Text("\(book.author)")
                .font(.system(size:15))
            
            Toggle("Add to Reading List", isOn: $book.readingList)
                .padding()
        
        
        switch synopsisLoader.state  {
            case .idle:Color.clear
            case .loading:ProgressView()
            case .failed: Text("Could not load synopsis")
            case .success (let synopsis):
                Text(synopsis)
                    .font(.system(size:15))
        }
    }
    .toolbar{
        ToolbarItem(placement:.topBarTrailing){
            Button(action: {book.readingList.toggle()}) {
                Text(book.readingList ? "Remove from Reading List": "Add to Reading List")
            }
        }
    }
        .task {
            await synopsisLoader.loadSynopsis(id: book.id)
        }
        .frame(width:300, height:500)
    }
}

#Preview {
    let preview = PreviewContainer ([Book.self])
    let book = Book.previewData[2]
    
    return BookDetail(book: book, synopsisLoader: SynopsisLoader(apiClient: BookAPIClient())).modelContainer(preview.container).padding()
}
