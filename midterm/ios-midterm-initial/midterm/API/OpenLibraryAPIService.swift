//
//  OpenLibraryAPIService.swift
//  midterm
//
//  Created by Casey Goldstein on 3/4/24.
//

import Foundation

struct BookAPIClient:APIClient{
    let session: URLSession = .shared
    
    func fetchSynopsis(id:String) async throws -> String {
        let path = OpenLibraryEndpoint.path(id: id)
        let response: OpenLibraryResponse = try await performRequest(url:path)
        return response.responseContainer.synopsis
    }
}
