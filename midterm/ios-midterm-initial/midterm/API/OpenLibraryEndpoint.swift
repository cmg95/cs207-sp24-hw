//
//  OpenLibraryEndpoint.swift
//  midterm
//
//  Created by Casey Goldstein on 3/4/24.
//

import Foundation

struct OpenLibraryEndpoint {
    static let baseUrl = "https://openlibrary.org/works"
    
    static func path(id:String) -> String {
        return "\(baseUrl)/\(id).json"
    }
}
