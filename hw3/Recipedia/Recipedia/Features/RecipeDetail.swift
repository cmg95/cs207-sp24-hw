import SwiftUI

struct RecipeDetail: View {
    
    @ObservedObject var recipe: Recipe
    @State var scale: Double = 1.0 // Scale is now of type Double with a default value of 1.0
    
    func cuisineString() -> String {
        switch recipe.name {
        case "Gong Bao Chicken With Peanuts":
            return "Chinese - Sichuan"
        case "Green Beans with Miso Butter":
            return "Japanese"
        case "Dry-fried Green Beans":
            return "Chinese - Sichuan"
        case "Rigatoni with Beef and Onion Ragu":
            return "Italian, pair with a taurasi"
        default:
            return "Cuisine not found"
        }
    }
    
    var body: some View {
        ScrollView {
            VStack {
                // Recipe Image
                AsyncImage(url: recipe.thumbnailUrl) { image in
                    image
                        .resizable()
                        .scaledToFit()
                        .cornerRadius(6)
                        .frame(maxWidth: 100, maxHeight: 100)
                } placeholder: {
                    if recipe.thumbnailUrl != nil {
                        ProgressView()
                    } else {
                        Image(systemName: "photo")
                    }
                }

                // Recipe Title
                Text(recipe.name)
                    .font(.title)

                // Button for lastPreparedAt
                Button(action: {
                    recipe.lastPreparedAt = recipe.lastPreparedAt == nil ? Date() : nil
                }) {
                    Text("Previously Prepared")
                    Image(systemName: recipe.lastPreparedAt != nil ? "checkmark.circle.fill" : "circle")
                        .foregroundColor(recipe.lastPreparedAt != nil ? .green : .gray)
                }

                // Cuisine and associated value
                HStack {
                    Text("Cuisine:")
                    Text(cuisineString())
                }

                HStack {
                    Text("Scale your recipe:")
                        .foregroundColor(.secondary) // Optional: Make the label a secondary color
                    Picker("Scale the recipe", selection: $scale) {
                        Text("0.5").tag(0.5)
                        Text("1").tag(1.0)
                        Text("2").tag(2.0)
                    }
                    .pickerStyle(.menu)
                }

                // Ingredients
                Spacer(minLength: 50)
                Text("Ingredients:").bold()
                ForEach(recipe.unsectionedIngredients) { ingredient in
                    // Adjust quantity based on the scale and only show if quantity is greater than 0
                    let quantityText = ingredient.quantity.map {
                        $0 > 0 ? String(format: "%.1f", $0 * scale) + " \(ingredient.unit ?? "") " : ""
                    } ?? ""
                    Text("\(quantityText)\(ingredient.ingredient.name)")
                }

                // Display ingredients with sections
                if let sectionLabels = recipe.sectionLabels {
                    ForEach(sectionLabels, id: \.self) { section in
                        Text(section.uppercased()).bold()
                        ForEach(recipe.ingredientsForSectionLabel(section)) { sectionIngredient in
                            // Adjust quantity based on the scale and only show if quantity is greater than 0
                            let quantityText = sectionIngredient.quantity.map {
                                $0 > 0 ? String(format: "%.1f", $0 * scale) + " \(sectionIngredient.unit ?? "") " : ""
                            } ?? ""
                            Text("\(quantityText)\(sectionIngredient.ingredient.name)")
                        }
                    }
                }
                
                Spacer(minLength: 50)
                Text("Instructions:").bold()
                ForEach(recipe.instructions.sorted(by: { $0.position < $1.position })) { instruction in
                    Text(instruction.instructionText)
                        .padding(.bottom) // Adds space after each instruction
                }

                // Edit notes field
                TextField("Notes", text: $recipe.notes)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
            }
            .padding()
        }
    }
}

// Preview provider
struct RecipeDetail_Previews: PreviewProvider {
    static var previews: some View {
        RecipeDetail(recipe: Recipe.previewData[0])
    }
}
