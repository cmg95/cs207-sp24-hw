//
//  RecipeList.swift
//  Recipedia
//
//  Created by Casey Goldstein on 2/12/24.
//

import Foundation

import SwiftUI
import SwiftData


struct RecipeList: View {
    @Query(sort: \Recipe.name) let recipes: [Recipe]
    @Environment(\.modelContext) private var modelContext
    var body: some View {
            NavigationView {
                List(recipes) { recipe in
                    NavigationLink(destination: RecipeDetail(recipe: recipe)) {
                        RecipeRow(recipe: recipe)
                    }
                }
                .navigationTitle("Recipes - cmg95")
            }    .onAppear {
                if recipes.isEmpty{
                    for recipe in Recipe.previewData{
                        modelContext.insert(recipe)
                    }
                }
            }
    }

}


#Preview {
 let preview = PreviewContainer([Recipe.self])
 let recipe = Recipe.previewData[0]
 return NavigationStack {
 RecipeDetail(recipe: recipe)
 .modelContainer (preview.container)
 }
    
}
