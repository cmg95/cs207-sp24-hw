//
//  RecipeRow.swift
//  Recipedia
//
//  Created by Casey Goldstein on 2/12/24.
//

import SwiftUI

struct RecipeRow: View {
    let recipe: Recipe

    var body: some View {
        HStack {
            AsyncImage(url: recipe.thumbnailUrl) {
              $0
                .resizable()
                .scaledToFit()
                .cornerRadius(6)
                .frame(maxWidth: 100, maxHeight: 100)
            } placeholder: {
              if recipe.thumbnailUrl != nil {
                ProgressView()
              } else {
                Image(systemName: "film.fill")
              }
            }
            
            VStack(alignment: .leading) {
                Text(recipe.mealCourse.rawValue.uppercased())
                    .font(.system(size: 14, weight: .regular))
                Text(recipe.name)
                    .font(.system(size: 18, weight: .heavy))
                Spacer()
            }
            
            Spacer()
            Image(systemName: recipe.lastPreparedAt != nil ? "checkmark.circle.fill" : "circle")
              .foregroundColor(Color.black)
        }
    }
}

#Preview {
    RecipeRow(recipe: Recipe.previewData[0])
}
