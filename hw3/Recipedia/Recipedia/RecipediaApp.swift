//
//  RecipediaApp.swift
//  Recipedia
//
//  Created by Casey Goldstein on 2/12/24.
//

import SwiftUI

@main
struct RecipediaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }.modelContainer(for: [Recipe.self])
    }
}
