import SwiftUI

struct RecipeDetail: View {
    @Bindable var recipe: Recipe
    @State var scalingFactor: Double = 1.0
    @State private var isPresentingRecipeForm: Bool = false
    @State private var editRecipeFormData: Recipe.FormData = Recipe.FormData()
    
    var body: some View {
        ScrollView {
            VStack {
                AsyncImage(url: recipe.thumbnailUrl) { image in
                    image
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(maxWidth: 200, maxHeight: 200)
                } placeholder: {
                    if recipe.thumbnailUrl != nil {
                        ProgressView()
                    } else {
                        Image(systemName: "fork.knife")
                    }
                }
                Spacer()
                Text(recipe.name)
                    .bold()
                Spacer()
                HStack {
                    Button("Previously Prepared") {
                        if recipe.lastPreparedAt != nil {
                            recipe.lastPreparedAt = nil
                        } else {
                            recipe.lastPreparedAt = Date.now
                        }
                    }
                    if recipe.lastPreparedAt != nil {
                        Image(systemName: "checkmark.circle.fill")
                            .foregroundColor(.blue)
                    } else {
                        Image(systemName: "circle")
                            .foregroundColor(.blue)
                    }
                }
                HStack {
                    Text("Scale the recipe:")
                        .bold()
                    Picker("Scale the recipe:", selection: $scalingFactor) {
                        Text("0.5").tag(0.5)
                        Text("1").tag(1.0)
                        Text("2").tag(2.0)
                    }
                    .pickerStyle(.menu)
                }
                
                Spacer()
                RecipeIngredientInstructions(recipe: recipe, scalingFactor: scalingFactor)
                Text("Notes")
                    .bold()
                TextEditor(text: $recipe.notes)
                    .frame(height: 200)
            }
        }
        .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
                Button("Edit") {
                    editRecipeFormData = recipe.dataForForm
                    isPresentingRecipeForm.toggle()
                }
            }
        }        .sheet(isPresented: $isPresentingRecipeForm) {
            NavigationStack {
                RecipeForm(data: $editRecipeFormData)
                    .toolbar {
                        ToolbarItem(placement: .navigationBarLeading) {
                            Button("Cancel") { isPresentingRecipeForm.toggle() }
                            
                        }
                        ToolbarItem(placement: .navigationBarTrailing) {
                            Button("Save") {
                                Recipe.update(recipe, from: editRecipeFormData)
                                isPresentingRecipeForm.toggle()
                            }
                        }
                    }
            }
        }
    }
    
}

struct RecipeIngredientInstructions: View {
    let recipe: Recipe
    let scalingFactor: Double
    
    var body: some View {
        VStack(alignment: .leading) {
            ForEach(recipe.unsectionedIngredients) { ingredient in
                Text(ingredientDisplay(ingredient, scalingFactor: scalingFactor))
            }
            Spacer()
            ForEach(recipe.sectionLabels, id: \.self) { sectionLabel in
                VStack(alignment: .leading) {
                    Text(sectionLabel)
                        .bold()
                    ForEach(recipe.ingredientsForSectionLabel(sectionLabel: sectionLabel)) { ingredient in
                        Text(ingredientDisplay(ingredient, scalingFactor: scalingFactor))
                    }
                    Spacer()
                }
                
            }
            ForEach(recipe.instructions) { instruction in
                Text(instruction.instructionText)}
        }
        .padding()
    }
}

func ingredientDisplay(
    _ recipeIngredient: RecipeIngredient, scalingFactor: Double) ->
String {
    var returnString = ""
    if let quantity = recipeIngredient.quantity {
        returnString.append(String(quantity * scalingFactor) + " ")
    }
    if let unit = recipeIngredient.unit {
        returnString.append(String(unit) + " ")
    }
    returnString.append(recipeIngredient.ingredient.name)
    return returnString
}

#Preview {
    let preview = PreviewContainer([Recipe.self])
    let recipe = Recipe.previewData[0]
    return NavigationStack {
        RecipeDetail(recipe: recipe)
            .modelContainer (preview.container)
    }
}

