import SwiftUI
import SwiftData

struct RecipeList: View {
    
    @Environment(\.modelContext) private var modelContext
    @Query (sort: \Recipe.name) private var recipes: [Recipe]
    @State var isPresentingRecipeForm: Bool = false
    @State private var newRecipeFormData = Recipe.FormData()
    
    var body: some View {
        NavigationStack {
            List(recipes) { recipe in
                NavigationLink(destination: RecipeDetail(recipe: recipe)) { RecipeRow(recipe: recipe) }
            }
            .navigationTitle("Recipes - cmg95")
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button("Add") {
                        isPresentingRecipeForm.toggle()
                    }
                }
            }
            .sheet(isPresented: $isPresentingRecipeForm) {
                NavigationStack {
                    RecipeForm(data: $newRecipeFormData)
                        .toolbar {
                            ToolbarItem(placement: .navigationBarLeading) {
                                Button("Cancel") { isPresentingRecipeForm.toggle()
                                    newRecipeFormData = Recipe.FormData()
                                }
                            }
                            ToolbarItem(placement: .navigationBarTrailing) {
                                Button("Save") {
                                    Recipe.create(from: newRecipeFormData, context: modelContext)
                                    newRecipeFormData = Recipe.FormData()
                                    isPresentingRecipeForm.toggle()
                                }
                            }
                        }
                        .navigationTitle("Add Recipe")
                }
            }
        }
        .onAppear {
            if recipes.isEmpty {
                for recipe in Recipe.previewData {
                    modelContext.insert(recipe)
                }
            }
        }
    }
}

struct RecipeRow: View {
    let recipe: Recipe
    var body: some View {
        
        HStack(alignment: .top) {
            AsyncImage(url: recipe.thumbnailUrl) { image in
                image
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .cornerRadius(6)
                    .frame(maxWidth: 95, maxHeight: 100)
                    
            } placeholder: {
                if recipe.thumbnailUrl != nil {
                    ProgressView()
                } else {
                    Image(systemName: "fork.knife")
                }
                
            }
            VStack(alignment: .leading) {
                Text(recipe.mealCourse.rawValue.uppercased())
                Text(recipe.name)
                    .bold()
                    .font(.title3)
            }
            Spacer()
            VStack {
                Spacer()
                Image(systemName: recipe.lastPreparedAt != nil ? "checkmark.circle.fill" : "circle")
                
                    .foregroundColor(recipe.lastPreparedAt != nil ? Color.green : Color.black)
                Spacer()
            }
        }
    }
}










#Preview {
    let preview = PreviewContainer([Recipe.self])
    preview.add(items: Recipe.previewData)
    return NavigationView {
        RecipeList()
            .modelContainer (preview.container)
    }}
