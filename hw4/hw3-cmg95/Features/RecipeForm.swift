import SwiftUI

struct RecipeForm: View {
    @Binding var data: Recipe.FormData
    
    
    var body: some View {
        Form {
            
            
            TextFieldWithLabel(label: "Name", text: $data.name, prompt: "Enter a Name")
            TextFieldWithLabel(label: "Details", text: $data.details, prompt: "Enter Details")
            TextFieldWithLabel(label: "Credit", text: $data.credit, prompt: "Enter Credit")
            Picker(selection: $data.mealCourse, label: Text("Meal Course")) {
                ForEach(Recipe.MealCourse.allCases) { mealCourse in
                    Text(mealCourse.rawValue)
                }
                
            }
            TextFieldWithLabel(label: "Thumbnail URL", text: $data.thumbnailUrl, prompt: "Enter a Thumbnail URL")
            
            Toggle("Previously Prepared", isOn: $data.previouslyPrepared)
            if data.previouslyPrepared {
                DatePicker("Last Prepared", selection: $data.lastPreparedAt)
            }
            TextFieldWithLabel(label: "Notes", text: $data.notes, prompt: "Enter Notes")
        }
    }
}

#Preview {
    let preview = PreviewContainer([Recipe.self])
    let data = Binding.constant(Recipe.previewData[0].dataForForm)
    return RecipeForm(data: data)
        .modelContainer(preview.container)
}
