import SwiftUI
import SwiftData

@main
struct hw3_cmg95App: App {
    var body: some Scene {
        WindowGroup {
            RecipeList()
                .modelContainer(for: Recipe.self)
        }
    }
}
