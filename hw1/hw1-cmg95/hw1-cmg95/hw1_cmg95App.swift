//
//  hw1_cmg95App.swift
//  hw1-cmg95
//
//  Created by Casey Goldstein on 1/30/24.
//

import SwiftUI

@main
struct hw1_cmg95App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
