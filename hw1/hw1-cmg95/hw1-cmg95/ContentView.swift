//
//  ContentView.swift
//  hw1-cmg95
//
//  Created by Casey Goldstein on 1/30/24.
//

import SwiftUI

struct ContentView: View {
    @State var cardEvents: [Achievement] = []
    @State var cardLength = 3
    
    var body: some View {
        VStack {
            Text("Play Classroom Bingo")
                .font(.largeTitle)
                .padding(.bottom,50)
            Stepper("Number Events",value:$cardLength)
            Button("Get New Card (\(cardLength) events)"){
                createCard()
            }
            Spacer()
            ForEach(cardEvents) { event in
                HStack{
                    Button(event.prompt){
                        event.achieved.toggle()
                    }
                    Spacer()
                    if (event.achieved){
                        Image(systemName:"checkmark")
                    } else {
                        Image(systemName:"circle")
                    }
                }
            }
            
            Spacer()
            
            var bingoAchieved: Bool = cardEvents.filter {$0.achieved == true}.count >= cardLength
            
            if (bingoAchieved){
                Text("Bingo!")
                    .font(.largeTitle)
                    .bold()
                    .foregroundColor(.red)
            } else{
                Text("no bingo yet")
            }
        }
        .padding()
    }
   func createCard(){
        cardEvents = Achievement.createCard(number:cardLength)
    }
}

#Preview {
    ContentView()
}
