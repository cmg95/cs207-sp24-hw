//
//  TopHeadlinesList.swift
//  news-app
//
//  Created by Casey Goldstein on 2/26/24.
//

import SwiftUI

struct TopHeadlinesList: View {
    let topHeadlinesLoader: TopHeadlinesLoader
    
    
    var body: some View {
        VStack {
            switch topHeadlinesLoader.state {
            case .idle: Color.clear
            case .loading: ProgressView()
            case .failed(let error): Text("Error: \(error.localizedDescription)")
            case .success(let articles):
                ArticleList(articles: articles)
            }
        }
        .task { await topHeadlinesLoader.loadHeadlinesData()
        }
    }
}

#Preview {
        TopHeadlinesList(topHeadlinesLoader: TopHeadlinesLoader(apiClient: MockNewsAPIClient()))
}
