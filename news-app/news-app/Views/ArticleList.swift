import SwiftUI
import Foundation

struct ArticleList: View {
    let articles: [Article]
    
    var body: some View {
        List(articles) { article in
            VStack {
                AsyncImage(url: URL(string: article.urlToImage ?? "")) {
                    $0
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .cornerRadius(6)
                } placeholder: {
                    if article.urlToImage != nil {
                        ProgressView()
                    } else {
                        EmptyView()
                    }
                }
                Text(article.title ?? "No Title")
                    .font(.system(size: 22))
                    .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                    .padding(.horizontal)
                    .padding(.top, 8)

                if let description = article.description {
                    Text(description)
                        .padding(.horizontal)
                        .padding(.top, 4)
                        .padding(.bottom, 8)
                }
            }
        }
    }
}

