//
//  SearchedArticlesList.swift
//  news-app
//
//  Created by Casey Goldstein on 2/27/24.
//

import SwiftUI

struct SearchedArticlesList: View {
    let searchedArticlesLoader: EverythingLoader
    
    @State var searchTerm: String = ""
    var body: some View {
        
        TextFieldWithLabel(label: "Search Term", text: $searchTerm, prompt: "Search for articles").padding()
        Button("Get Articles") {
          Task {
              await searchedArticlesLoader.loadEverythingData(searchTerm: searchTerm)
          }
        }
        .disabled(searchTerm == "")
        
        VStack {
            switch searchedArticlesLoader.state {
            case .idle: Color.clear
            case .loading: ProgressView()
            case .failed(let error): Text("Error: \(error.localizedDescription)")
            case .success(let articles):
                ArticleList(articles: articles)
            }
        }
    }
}



#Preview {
    SearchedArticlesList(searchedArticlesLoader: EverythingLoader(apiClient: MockNewsAPIClient()))
}
