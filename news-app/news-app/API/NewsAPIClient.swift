//
//  NewsAPIClient.swift
//  weather-app
//
//  Created by Casey Goldstein on 2/25/24.
//

import Foundation
import CoreLocation

protocol NewsAPI {
    func fetchTopHeadlines() async throws -> [Article]
    func fetchEverything(searchTerm: String) async throws -> [Article]
}

struct NewsAPIClient: NewsAPI, APIClient {
    let session: URLSession = .shared
    
    func fetchTopHeadlines() async throws -> [Article] {
        let path = NewsAPIEndpoint.path(queryType: .topHeadlines, searchTerm: "")
        let response: NewsResponse = try await performRequest(url: path)
        return response.articles
    }
    
    func fetchEverything(searchTerm: String) async throws -> [Article] {
        let path = NewsAPIEndpoint.path(queryType: .everything(searchTerm: searchTerm), searchTerm: searchTerm)
        let response: NewsResponse = try await performRequest(url: path)
        return response.articles
    }
}

struct MockNewsAPIClient: NewsAPI {

    func fetchTopHeadlines() async throws -> [Article] {
        Article.mock()
    }

    func fetchEverything(searchTerm: String) async throws -> [Article] {
        Article.mock()
    }
}
