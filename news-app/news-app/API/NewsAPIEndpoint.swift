//
//  NewsAPIEndpoint.swift
//  news-app
//
//  Created by Casey Goldstein on 2/25/24.
//

import Foundation
import CoreLocation

struct NewsAPIEndpoint {
    static let baseUrl = "https://newsapi.org/v2"
    static let apiKey = "257fbe04aacd4d0bb1638aa549e35c87"

    enum QueryType {
      case topHeadlines
      case everything(searchTerm: String)

      var queryName: String {
          switch self {
            case .topHeadlines: return "top-headlines"
            case .everything: return "everything"
          }
      }
    }

    static func path(queryType: QueryType, searchTerm: String) -> String {
        let url = "\(baseUrl)/\(queryType.queryName)"
        let key = "apiKey=\(apiKey)"
        switch queryType {
            case .topHeadlines: return "\(url)?country=us&\(key)"
            case .everything(let searchTerm): 
            if let encodedSearchTerm = searchTerm.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                return "\(url)?q=\(encodedSearchTerm)&\(key)"
            } else {
                return "\(url)?q=&\(key)"
            }
        }
    }
}
