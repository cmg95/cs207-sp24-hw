//
//  NewsLoader.swift
//  weather-app
//
//  Created by Casey Goldstein on 2/26/24.
//

import Foundation
import SwiftUI

@Observable
class TopHeadlinesLoader {
    var articles: [Article] = []
    let apiClient: NewsAPI
    private(set) var state: LoadingState = .idle

    enum LoadingState {
        case idle
        case loading
        case success(data: [Article])
        case failed(error: Error)
    }

    init(apiClient: NewsAPI) {
        self.apiClient = apiClient
    }

    @MainActor
    func loadHeadlinesData() async {
        self.state = .loading
        do {
            articles = try await apiClient.fetchTopHeadlines()
            self.state = .success(data: articles)
        } catch {
            self.state = .failed(error: error)
        }
    }
}
