//
//  News.swift
//  news-app
//
//  Created by Casey Goldstein on 2/25/24.
//

import Foundation

struct NewsResponse: Decodable {
    var status: String
    var totalResults: Int
    var articles: [Article]
}

struct Article: Decodable, Identifiable {
    var id: String {
        return url
    }
    var source: Source
    var author: String?
    var title: String?
    var description: String?
    var url: String
    var urlToImage: String?
    var publishedAt: String
    var content: String?
}

extension Article {
    static func mock() -> [Article] {
        [Article(source: Source(id: "1", name: "Variety"), author: "William Earl", title: "Spirit Awards 2024 Winners List: Updating Live - Variety", description: "Check out the full list of winners at the 2024 Independent Spirit Awards.", url: "https://variety.com/2024/awards/news/2024-independent-spirit-awards-winners-list-1235922179", urlToImage: "https://variety.com/wp-content/uploads/2024/02/GettyImages-2038598153.jpg?crop=0px%2C0px%2C5000px%2C2813px&resize=1000%2C563", publishedAt: "1970-01-01T00:00:00Z", content: "The 2024 Independent Spirit Awards are taking place on Sunday at the traditional Santa Monica beach tent location, with Aidy Bryant set to host. “Past Lives,” “May December” and “American Fiction” ar… [+8566 chars]")]
    }
}

struct Source: Decodable {
    var id: String?
    var name: String
}
