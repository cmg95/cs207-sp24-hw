//
//  news_appApp.swift
//  news-app
//
//  Created by Casey Goldstein on 2/26/24.
//

import SwiftUI

@main
struct news_appApp: App {
    var body: some Scene {
        WindowGroup {
            TabContainer()
        }
    }
}
