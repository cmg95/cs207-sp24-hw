//
//  TabContainer.swift
//  news-app
//
//  Created by Casey Goldstein on 2/26/24.
//

import SwiftUI

struct TabContainer: View {
    @State private var selectedTab: Tab = .topHeadlines
    
    enum Tab {
        case topHeadlines
        case search
    }
    var body: some View {
        Group {
            TabView(selection: $selectedTab) {
                NavigationStack {
                    TopHeadlinesList(topHeadlinesLoader: TopHeadlinesLoader(apiClient: NewsAPIClient()))
                }
                .tabItem {
                    Label("Top Headlines", systemImage: "newspaper")
                        .accessibility(label: Text("Top Headlines"))
                }
                .tag(Tab.topHeadlines)
                
                NavigationStack {
                    SearchedArticlesList(searchedArticlesLoader: EverythingLoader(apiClient: NewsAPIClient()))
                }
                .tabItem {
                    Label("Search", systemImage: "magnifyingglass")
                        .accessibility(label: Text("Search"))
                }
                .tag(Tab.topHeadlines)
            }
        }
    }
}
