//
//  ContentView.swift
//  hw0-cmg95
//
//  Created by Casey Goldstein on 1/19/24.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundStyle(.tint)
            Text("Hello, CS207!")
            Text("cmg95 is here!")
        }
        .padding()
    }
}

#Preview {
    ContentView()
}
