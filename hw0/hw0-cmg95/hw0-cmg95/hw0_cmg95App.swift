//
//  hw0_cmg95App.swift
//  hw0-cmg95
//
//  Created by Casey Goldstein on 1/19/24.
//

import SwiftUI

@main
struct hw0_cmg95App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
